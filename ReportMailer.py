'''
Created on Jan 3, 2013

@author: sreenm
'''


 
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

import smtplib
import os
import glob 
from datetime import datetime 

#import email,email.encoders,email.mime.text,email.mime.base
  
class ReportMailer(object):
    '''
        Mails the generated  Report as it is generated out to a list of people
    '''

    def __init__(self, fileName=None):
        '''
        Constructor
        '''
        self.reportFileLocation = "c://tmp//"
        self.reportFile =  'ICEGasReport_'  + datetime.today().strftime('%Y-%m-%d') + ".csv"
        self.reportFileName = self.reportFileLocation + self.reportFile if fileName == None else fileName
        
    def sendReport(self , listOfEmailIds='murali.sreenivasan@cci.com', filePathForReport='c:\\tmp\\' ):
        smtpserver = 'mail.ldhenergy.net'   
        tolist = [ 'murali.sreenivasan@cci.com']#, 'Christopher.Crawford@cci.com']
        fromAddr = 'ETOOLS-Admin@cci.com'   
        filelist = [] 
        
        if not os.path.exists(self.reportFileName):
            print "No Report to mail."
            return
            
        while len(filelist) == 0 : 
            filelist = glob.glob(self.reportFileName) 
        
        for fayil  in filelist :
            msg = MIMEMultipart()
            msg['Subject'] = 'ICE Gas Report for :' + datetime.today().strftime(' %d %b %Y ') 
            msg['From'] = fromAddr
            
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(fayil).read())
            Encoders.encode_base64(part)

            print fayil
            
            part.add_header('Content-Disposition', 'attachment; filename="%s"' %(fayil.split("//")[-1]))
            
            msg.attach(part)
            
            s = smtplib.SMTP(smtpserver)
            for to in tolist : 
                msg['To'] = to
                s.sendmail(fromAddr , to, msg.as_string())
            s.quit()




if __name__ == "__main__":
    reportMailer = ReportMailer() 
    reportMailer.sendReport()
    
    