'''
Created on Jan 3, 2013

@author: sreenm
'''

import urllib
import urllib2
import BeautifulSoup
import re
import traceback
import sys 
from datetime import datetime
import ReportMailer
import dateutil.parser

url = 'https://www.theice.com/marketdata/indices/gasResults.do'

values = {'indexType' : '1',
          'hubChoice' : 'All',
          'startDay' : '2',
          'startMonth' : '0',
          'startYear' : '2013',
          'endDay' : '2',
          'endMonth' : '0',
          'endYear':'2013'
           }
reportFileLocation = "c://tmp//"
reportFile = "ICEGasReport_" + datetime.today().strftime('%Y-%m-%d')  + ".csv"
 
class ICEGasReport(object):
    
    def __init__(self, fileName=None, startDay=None, startMonth=None, endDay=None, endMonth=None, year=None):
        today = datetime.today()
        self.reportFilename = reportFileLocation + reportFile if fileName == None else fileName
        values['startDay'] = today.day  if startDay == None else startDay
        values['startMonth'] = today.month - 1 if startMonth == None else startMonth
        values['endDay'] = today.day  if endDay == None else endDay
        values['endMonth'] = today.month - 1 if endMonth == None else endMonth 
        values['startYear'] = today.year if year == None else year
        values['endYear'] = today.year if year == None else year
        
        
    def condense(self, s):
        s = re.sub(r"\s+", " ", s, re.DOTALL);  #$s =~ s/\s+/\ /gs;
        return s.strip()

    def stripTags(self, s):
        s = re.sub(r"\<span\s+style\s*\=\s*\"display\:none[^\"]*\"[^\>]*\>[^\<]*\<\/span\>", "", s)
        s = re.sub(r"\&\#160\;", " ", s)
        return self.condense(re.sub(r"\<[^\>]*\>", " ", s))

    def generateReport(self):
        try:
            formData = urllib.urlencode(values)
            request = urllib2.Request(url, formData)
            response = urllib2.urlopen(request)
            responseHtml = response.read()
            soup = BeautifulSoup.BeautifulSoup(responseHtml)

            tables = soup.findAll("table")  
            if len(tables) > 2:
                table = tables[2]

                localfile = open(self.reportFilename, 'w')

                for r in table.findAll('tr'):
                    rl = []
                    columnCount = 0
                    for c in r.findAll(re.compile('td|th')):
                        if columnCount in [1, 2, 3]:
                            try:
                                dateValue = dateutil.parser.parse(self.stripTags(c.renderContents()), ignoretz=True)
                                rl.append(dateValue.strftime('%m/%d/%Y'))
                            except ValueError:
                                rl.append(self.stripTags(c.renderContents()))
                        else:
                            rl.append(self.stripTags(c.renderContents()))
                        columnCount += 1
                    localfile.write(" , ".join(rl))
                    localfile.write("\n")
            else:
                print "Report Data not available for selected date"
        except Exception as exception:
            traceback.print_exc(file=sys.stdout)
            

    
if __name__ == '__main__':
    gasReport = ICEGasReport() #reportFileLocation + reportFile)
    gasReport.generateReport()
    #reportMailer = ReportMailer.ReportMailer()
    #reportMailer.sendReport()